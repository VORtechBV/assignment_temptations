import numpy as np
from scipy.integrate import solve_ivp
import argparse



import numpy as np
import json
input_file = 'bobbie.json'
input = json.load(open(input_file, 'r'))

def fun(t, y):
    y01= np.array(y[0:2])
    y23= np.array(y[2:4])
    ab  = np.array([0.0, 0.0])

    for t in input['temptations']:
        pos=np.array((t['x'],t['y']))
        # decrease position with y01
        dx=pos - y01
        ab=ab+t['weight']*dx/np.linalg.norm(dx)   / np.linalg.norm(dx)**2*(1 - input['mindist'] / np.linalg.norm(dx)**2)
        #ab = ab + 0.3 * t['weight'] * dx /  np.linalg.norm(dx)**3  * (1 - input['mindist'] / np.linalg.norm(dx))


    ab = ab - min(input['friction_max'], input['friction_coeff'] * np.linalg.norm(y23)) * y23 / np.linalg.norm(y23)
    return np.concatenate((y23, ab))
import numpy as np
if input['temptations'][0]['name'] == 'goal':
    goal_position = np.array((input['temptations'][0]['x'], input['temptations'][0]['y']))
if input['temptations'][1]['name'] == 'goal':
    goal_position = np.array((input['temptations'][1]['x'], input['temptations'][1]['y']))
if input['temptations'][2]['name'] == 'goal':
    goal_position = np.array((input['temptations'][1]['x'], input['temptations'][2]['y']))
if input['temptations'][3]['name'] == 'goal':
    goal_position = np.array((input['temptations'][3]['x'], input['temptations'][3]['y']))


# initial vector
v0=input['initial_velocity']*goal_position/np.linalg.norm(goal_position)
# solve the ivp
solution= solve_ivp(fun,(0.0,input['tstop']),np.array([0.0, 0.0,v0[0], v0[1]]),'RK45',  np.arange(0.0,input['tstop'],   input['tstep']),rtol = 1e-6)
#plot
# We use pdf backend, since we're only interested in saving, not showing
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
fig, ax = plt.subplots()
ax.axis('equal')
for t in input['temptations']:
    scale = 0.6
    radius = scale * np.sqrt(np.abs(t['weight']))
    color = 'red' if t['weight'] < 0.0 else 'blue'
    theta = np.linspace(0, 2 * np.pi, 50)

    x = t['x']+radius*np.sin(theta)
    y = t['y']+radius*np.cos(theta)
    ax.fill(x,y,alpha=0.3,color=color)

    # circle = plt.Circle(position(temptation), radius, color=color,
    #                     fill=True, alpha=0.3, clip_on=True)
    # ax.add_artist(circle)
    ax.annotate(t['name'],xy=(t['x'],t['y']))
x,y = solution['y'][0],solution['y'][1]
ax.plot(x, y)

import pathlib
p = pathlib.Path(input_file)
pdf_filename = f'{p.parent}/{p.stem}_result.pdf'
fig.savefig(pdf_filename)


print('Done')
